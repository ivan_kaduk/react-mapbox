window.onload = function () {

  class Map extends React.Component {

    static propTypes = {
      id: PropTypes.string.isRequired,
      mapboxAPIToken: PropTypes.string.isRequired,
      center: PropTypes.shape({
        lat: PropTypes.number,
        lon: PropTypes.number,
      }),
      zoom: PropTypes.number,
      onMapPositionChange: PropTypes.func.isRequired,
    }

    static defaultProps = {
      center: {
        lat: 0,
        lon: 0,
      },
      zoom: 9,
    }

    rebuildMap(props) {
      this.map = undefined;
      this.ref.innerHTML = '';
      this.renderMap(props);
    }

    renderMap(props) {
      const {
        id,
        mapboxAPIToken,
        center: {
          lat,
          lon,
        },
        zoom,
      } = props;

      mapboxgl.accessToken = mapboxAPIToken;

      if (this.map !== undefined) {
        if (this.map.transform._zoom != zoom) {
          this.map.zoomTo(zoom);
        } else {
          this.map.flyTo({ center: [lat, lon] });
        }
        return;
      }

      try {
        this.ref.className = 'preloader';
        this.map = new mapboxgl.Map({
          container: id,
          style: 'mapbox://styles/mapbox/streets-v11',
          center: [lat, lon],
          zoom,
        });

        this.map.on('load', () => {
          this.ref.className = '';
          this.map.loadImage(
            'https://img.icons8.com/material/96/000000/cloudflare.png',
            (error, image) => {
              if (error) throw error;
              this.map.addImage('cursor', image);
              this.map.addLayer({
                "id": "points",
                "type": "symbol",
                "source": {
                  "type": "geojson",
                  "data": {
                    "type": "FeatureCollection",
                    "features": [{
                      "type": "Feature",
                      "geometry": {
                        "type": "Point",
                        "coordinates": [lat, lon]
                      }
                    }]
                  }
                },
                "layout": {
                  "icon-image": "cursor",
                  "icon-size": 1
                }
              });
            });
        });

        this.map.on('moveend', function (e) {
          const center = e.target.transform._center;

          e.target.getSource('points').setData({
            "type": "FeatureCollection",
            "features": [{
              "type": "Feature",
              "geometry": {
                "type": "Point",
                "coordinates": [center.lng, center.lat]
              }
            }]
          });

          if (typeof this.onMapPositionChange === 'function') {
            this.onMapPositionChange(e);
          }

        });

      } catch (e) {
        console.log(e);
      }
    }

    componentDidMount() {
      this.renderMap(this.props);
    }

    componentDidUpdate(prevProps) {
      const isPropsChanged = !Object.compare(this.props, prevProps);
      if (isPropsChanged) {
        const isSizeChanged = !Object.compare(this.props.viewport, prevProps.viewport);
        const isTokenChanged = this.props.mapboxAPIToken !== prevProps.mapboxAPIToken;
        if (isTokenChanged) {
          this.rebuildMap(this.props);
        } else if (isSizeChanged) {
          this.ref.style.width = `${this.props.viewport.width}px`;
          this.ref.style.height = `${this.props.viewport.height}px`;
          this.map.resize()
        } else {
          this.renderMap(this.props);
        }
      }
    }

    render() {
      const {
        viewport: {
          width,
          height,
        },
      } = this.props;
      return <div
        style={{
          width,
          height,
        }}
        ref={ref => this.ref = ref}
        id={this.props.id}
      >
      </div>;
    }

  }

  class App extends React.Component {

    constructor(props) {
      super(props);

      const formInitState = {
        mapboxAPIToken: "pk.eyJ1IjoiYy1lZGl0b3IiLCJhIjoiY2sxaThqazVvMG15NzNucGM1eG43cmlsYSJ9.VcJUA3hkuWHQ-VQ2s76uQQ",
        center: {
          lat: -74.50,
          lon: 40,
        },
        zoom: 9,
        viewport: {
          width: 500,
          height: 500,
        },
      }

      const toolbarInitState = {
        isOpened: false,
      }

      this.state = {
        ...formInitState,
        ...toolbarInitState,
      }

      this.handleToggleToolbar = this.handleToggleToolbar.bind(this);
      this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleToggleToolbar(e) {
      this.setState({
        isOpened: !this.state.isOpened,
      });
    }

    handleFieldChange(e) {
      const fieldName = e.target.name;
      const fieldValue = e.target.value;
      const newState = Object.update(this.state, fieldName, fieldValue);
      this.setState(newState);
    }

    handleMapPositionChange(e) {
      console.log(e);
    }

    render() {

      const {
        id,
        mapboxAPIToken,
        center,
        zoom,
        viewport,
        isOpened,
      } = this.state;

      return <div className="map">
        <div className={`map-toolbar ${isOpened ? 'map-toolbar--opened' : ''}`}>
          <button
            className="map-toggle"
            onClick={this.handleToggleToolbar}
          >
            {isOpened ? '<' : '>'} Toolbar
          </button>
          <form className="map-form">
            Api Token
            <input name={'mapboxAPIToken'} value={mapboxAPIToken} onChange={this.handleFieldChange} />
            Lattitude
            <input type="number" name={'center.lat'} value={center.lat} onChange={this.handleFieldChange} />
            Longitude
            <input type="number" name={'center.lon'} value={center.lon} onChange={this.handleFieldChange} />
            Zoom
            <input type="number" name={'zoom'} value={zoom} onChange={this.handleFieldChange} />
            Width
            <input type="number" name={'viewport.width'} value={viewport.width} onChange={this.handleFieldChange} />
            Height
            <input type="number" name={'viewport.height'} value={viewport.height} onChange={this.handleFieldChange} />
          </form>
        </div>

        <Map
          id="newMap"
          mapboxAPIToken={mapboxAPIToken}
          center={center}
          zoom={zoom}
          onMapPositionChange={this.handleMapPositionChange}
          viewport={viewport}
        />

      </div>
    }
  }

  ReactDOM.render(<App />, document.getElementById('app'));

};
