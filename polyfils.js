Object.compare = function (obj1, obj2) {
  if (typeof obj1 === "object" && typeof obj2 === "object") {
    let isObjectMatch = false;
    for (let property1 in obj1) {
      let isPropertyMatch = false;
      for (let property2 in obj2) {
        if (property1 === property2) {
          isPropertyMatch = Object.compare(obj1[property1], obj2[property2])
        }
        if (isPropertyMatch) {
          break;
        }
      }
      isObjectMatch = isPropertyMatch;
      if (!isObjectMatch) {
        break;
      }
    }
    return isObjectMatch;
  } else {
    return obj1 === obj2;
  }
}

Object.update = function (object, path, data) {
  const pathSegments = path.split('.');
  const updatedObject = Object.assign({}, object);

  let current = updatedObject;

  pathSegments.forEach((segment, index) => {
    const isLastSegment = (pathSegments.length - 1) === index;
    if (!isLastSegment) {
      current[segment] = (typeof current[segment] === 'object') ?
        Object.assign({}, current[segment]) :
        current[segment];
      const isCurrentSegmentExist = !!current[segment];
      if (!isCurrentSegmentExist) {
        current[segment] = {};
      }
      current = current[segment];
    } else {
      current[segment] = data;
    }
  });

  return updatedObject;
}